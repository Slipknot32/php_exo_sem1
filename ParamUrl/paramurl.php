<?php
///////////////////////////////////// exo 1

index.php?nom=Nemare&prenom=Jean
echo $_GET['nom'] . "<br>";
echo $_GET['prenom'] . "<br>";

////////////////////////////////// exo 2

?nom=Nemare&prenom=Jean
if (isset ($_GET['age'])) {
echo 'la variable age existe et vaut : ' . $_GET['age'];
}
if(!isset($_GET['age'])) {
echo 'le querystring ne contient pas age';
}


////////////////////////////////// exo 3

// ?dateDebut=2/05/2016&dateFin=27/11/2016

if (isset ($_GET['dateDebut'])) {
echo 'la variable dateDebut existe et vaut : ' . $_GET['dateDebut']. "<br>";
}
else{
echo 'le querystring ne contient pas dateDebut' . "<br>";
}

if (isset ($_GET['dateFin'])) {
echo 'la variable dateFin existe et vaut : ' . $_GET['dateFin']. "<br>";
}
else{
echo 'le querystring ne contient pas dateFin'. "<br>";
}



////////////////////////////////// exo 4

// ?langage=PHP&serveur=LAMP

echo $_GET['langage'] . "<br>";
echo $_GET['serveur'] . "<br>";


////////////////////////////////// exo 5

// ?semaine=12
echo $_GET['semaine'] . "<br>";

////////////////////////////////// exo 6

// ?batiment=12&salle=101

echo $_GET['batiment'] . "<br>";
echo $_GET['salle'] . "<br>";

?>